# Bash Template Thingy

This is a simple template system written in bash. It substitutes templates of the form `{{ template }}` with environment variables.

## Usage

See the example file. This can be run in your terminal via `./example`.

You can also pipe to `bashlate` as follows:

`echo 'Hello {{ USER }}' | ./bashlate -`
