#!/usr/bin/env bats

@test "it should work" {
  f="$(./example)"
  x="$(cat <<EOF
#!./bashlate
My name is $USER

I live at $HOME

EOF
)"
  [[ -n "$f" ]]
  [[ "$f" == "$x" ]]
}
